**For convenience, please read this file from a markdown reader.**

# Testing Procedures **[ID-id]**

    Sebelum membahas skenario unit dan instrument testing yang akan dilakukan, Saya akan memberikan gambaran tentang konsep dari submission yang saya buat terlebih dahulu. Berikut ini konsep dari RNN Jetpack Pro S2 :

## Struktur File

Berlaku untuk file pada direktori `/app/src/main/java/rnn/practice/rnnjetpackpros2/`

* `.../adapters` : Adapter related classes
* `.../application` : Application class
* `.../fragments` : Fragments used within the app
* `.../interfaces` : Interface classes
* `.../models` : Data model classes
* `.../repository` : Repository related classes
* `.../routers` : Fuel Router classes to route requests
* `.../ui` : Activities used within the app
* `.../util` : Utility classes and objects
* `.../viewmodels` : Viewmodel related classes

## Konsep 

Aplikasi ini menggunakan **Fuel** sebagai HTTP Library dan **Kodein** sebagain Dependency Injector. Instance **Fuel** merupakan sebuah singleton class `.../util/RNNFuelInstance.kt` yang diinisiasi pada `.../application/AppDefinition.kt` oleh **Kodein**. Semua dependensi yang diinisasi dan tersedia akan di inject oleh **Kodein** dalam mode retrieval sehingga tidak dikonsumsi sebagai parameter class.

Untuk melakukan separasi request, **Fuel** memiliki fitur routing yang telah saya buat pada `.../routers/TMDBDataRouter.kt` dan `.../routers/TMDBImageRouter.kt` yang kemudian dikonsumsi oleh repository `.../repository/TMDBRepository` untuk menyediakan 4 method dasar yang diextend dari interface `.../interfaces/IServiceTMDB.kt`. Repository ini kemudian diinisiasikan pula oleh **Kodein** sebagai sebuah singleton yang memerlukan parameter *Context* dan objek *FuelInstance* yang telah diinisiasikan sebelumnya. 

Repository ini terdiri atas method : 

* `detailsMovie()` yang menerima parameter *String* yang mewakili ID dari sebuah film untuk memanggil detail informasi dari sebuah film.

* `detailsTVShow()` yang menerima parameter *String* yang mewakili ID dari sebuah acara tv untuk memanggil detail informasi dari sebuah acara tv.

* `searchMovies()` yang menerima parameter *String* yang mewakili kata kunci dari pencarian film untuk memanggil sejumlah hasil terkait pada database TMDB.

* `searchTVShow()` yang menerima parameter *String* yang mewakili kata kunci dari pencarian acara tv untuk memanggil sejumlah hasil terkait pada database TMDB.

Pada lapisan teratas repository ini dikonsumsi oleh *ViewModel* `.../viewmodels/TMDBViewModel.kt`. *ViewModel* ini diextend dari class *AndroidViewModel* yang memerlukan parameter *Application* yang turut disediakan oleh **Kodein** saat diinisiasikan sebagai sebuah singleton. Pada *ViewModel* ini tersedia 6 method dimana 4 diantaranya merupakan percabangan dari 2 method yang sama, dengan perbedaan diantara masing-masing dari percabangan tersebut yakni pada parameter yang diperlukan.

2 method pertama yaitu `queryMovie()` dan `queryTVShow()` merupakan method yang memanggil method `detailsMovie()` dan `detailsTVShow()` dari repository. 2 method sisanya merupakan `queryMovies()` dan `queryTVShows()` yang tersedia dalam bentuk non-callback-listener dan dengan callback-listener `(RNNState) -> Unit` sebagai parameter, yang jika dijumlah sama dengan 2 * 2 = 4 total method. Kedua method tersebut merupakan hasil *Function Overloading* yang pada intinya bercabang dari 2 method `searchMovies()` dan `searchTVShows()`.

Untuk kedua method pada *ViewModel* yang memerlukan parameter berupa callback listener ditujukan untuk penggunaan tertentu dimana perlu adanya callback ke Activity / Fragment yang memanggil method tersebut untuk memberikan interaksi UI seperti kondisi loading, kondisi selesai maupun kondisi fail. 

Kondisi - kondisi ini di broadcast oleh *Interface* `.../interfaces/IState.kt` pada *LogCat* dengan *TAG* RNNState dan dapat di observe pula perubahannya secara global melalui method `getState()` pada *ViewModel* (NOTE: Future plan untuk Submission 3). 

RNNState sendiri merupakan objek enum yang memiliki kondisi INIT, LOAD, DONE, dan FAIL.

## Unit Testing

Silakan merujuk pada file `/test/../ShowViewModelTest.kt` mengenai rincian nilai yang di jadikan acuan perbandingan. Terdapat pula komentar mengenai proses testing pada setiap baris pengujian pada file tersebut.

Karena pada S2 ini saya membuat semua method mengakses API TMDB secara asynchronous, saya memutuskan untuk melakukan mocking juga pada setiap pemanggilan method dengan data dummy yang di generate oleh function pada file `/test/../ShowViewModelTest.kt` ini juga.

NOTE: Pada S1 semua data merupakan data lokal.

* TMDBViewModelTest

    * simulate a single movie details query

        * Job Execution Check 
        > Apakah method terpanggil dan selesai ?

        * Movie ID Check 
        > Apakah nilai properti ID dari data yang dikembalikan sesuai ?

        * Movie Title Check 
        > Apakan nilai properti Title dari data yang dikembalikan sesuai ?

    * simulate a single tv shows details query

        * Job Execution Check 
        > Apakah method terpanggil dan selesai ?

        * TV Show ID Check 
        > Apakah nilai properti ID dari data yang dikembalikan sesuai ?

        * TV Show Title Check 
        > Apakan nilai properti Title dari data yang dikembalikan sesuai ?

    * simulate a movie search items retrieval

        * Job Execution Check 
        > Apakah method terpanggil dan selesai ?

        * Item Count Check 
        > Apakah jumlah item dari data yang dikembalikan sesuai ?

        * Page Count Check
        > Apakah jumlah halaman dari data yang dikembalikan sesuai ?

        * Page Index Check
        > Apakah indeks halaman yang sedang aktif dari data yang dikembalikan sesuai ?

        * Array Size Check (Properti dari TMDB, memang mirip seperti itemCount tapi diukur dari array hasil pencarian)
        > Apakan ukuran array item dari data yang dikembalikan sesuai ?

        * Movie ID Check 
        > Apakah nilai properti ID dari elemen ke n dari data yang dikembalikan sesuai ?

        * Movie Title Check 
        > Apakan nilai properti Title dari elemen ke n dari data yang dikembalikan sesuai ?

    * simulate a tv show search items retrieval

        * Job Execution Check 
        > Apakah method terpanggil dan selesai ?

        * Item Count Check 
        > Apakah jumlah item dari data yang dikembalikan sesuai ?

        * Page Count Check
        > Apakah jumlah halaman dari data yang dikembalikan sesuai ?

        * Page Index Check
        > Apakah indeks halaman yang sedang aktif dari data yang dikembalikan sesuai ?

        * Array Size Check (Properti dari TMDB, memang mirip seperti itemCount tapi diukur dari array hasil pencarian)
        > Apakan ukuran array item dari data yang dikembalikan sesuai ?

        * TV Show ID Check 
        > Apakah nilai properti ID dari elemen ke n dari data yang dikembalikan sesuai ?

        * TV Show Title Check 
        > Apakan nilai properti Title dari elemen ke n dari data yang dikembalikan sesuai ?

    * simulate a database related activities related to favorites feature for both movies and tv shows

        * Add Favorite Movie Job Execution Check 
        > Apakah method terpanggil dan selesai ?

        * Find Favorite Movie Job Execution Check 
        > Apakah method terpanggil dan data yang dikembalikan sesuai ?

        * Remove Favorite Movie Job Execution Check 
        > Apakah method terpanggil dan selesai ?

        * Add Favorite TV Show Job Execution Check 
        > Apakah method terpanggil dan selesai ?

        * Find Favorite TV Show Job Execution Check 
        > Apakah method terpanggil dan data yang dikembalikan sesuai ?

        * Remove Favorite TV Show Job Execution Check 
        > Apakah method terpanggil dan selesai ?

        * Reset Favorites Table Job Execution Check 
        > Apakah method terpanggil dan selesai ?

    * simulate a paging activity for favorites

        * Paging Data Stream & Execution Check 
        > Apakah method terpanggil dan data yang dikembalikan sesuai ?

* TMDBRepository

    * simulate searchMovies query

        * Method Execution Check 
        > Apakah method terpanggil dan memberikan return yang sesuai ?

    * simulate searchTVShows query

        * Method Execution Check 
        > Apakah method terpanggil dan memberikan return yang sesuai ?

    * simulate detailsMovie query

        * Method Execution Check 
        > Apakah method terpanggil dan memberikan return yang sesuai?

    * simulate detailsTVShow query

        * Method Execution Check 
        > Apakah method terpanggil dan memberikan return yang sesuai?

* RoomDBRepositoryTest

    * simulate an activity of adding a favorite item for both movie and tv show

        * Method Execution Check 
        > Apakah method terpanggil dan memberikan return yang sesuai untuk movie dan tv show (masing-masing) ?

    * simulate an activity of finding a favorite item for both movie and tv show

        * Method Execution Check 
        > Apakah method terpanggil dan memberikan return yang sesuai untuk movie dan tv show (masing-masing) ?

    * simulate an activity of removing a favorite item for both movie and tv show

        * Method Execution Check 
        > Apakah method terpanggil dan memberikan return yang sesuai untuk movie dan tv show (masing-masing) ?

    * simulate an activity of resetting table_favorites

        * Method Execution Check 
        > Apakah method terpanggil dan memberikan return yang sesuai?

    * simulate an activity of paging data for favorites

        * Method Execution Check 
        > Apakah method terpanggil dan memberikan return yang sesuai?

## Instrument Testing (Espresso)

Pengujian intrumental pada project ini terbagi atas 3 pengujian terpisah berdasarkan masing-masing *Activity* yakni `/androidTest/.../MainActivityTest.kt`, `/androidTest/.../DetailsMovieActivityTest.kt`, dan `/androidTest/.../DetailsTVShowActivityTest.kt`

* MainActivityTest

    * checkMenu() [-> `MainActivity`]

        <!-- MainActivity -->

        * BottomNavigationView Menu Check 
        > Apakah ketiga menu item BottomNavigationMenu `isDisplayed()` ?

    * checkSearchPageFragment() [-> `MainActivity/SearchPageFragment`]

        <!-- MainActivity -->

        * BottomNavigationView Menu Check 
        > Apakah menu item pada BottomNavigationMenu `isDisplayed()` ? bisa `click()` ?

        <!-- SearchPageFragment -->

        * Toolbar Menu Item Check 
        > Apakah menu item pada Toolbar `isDisplayed()` ? bisa `click()` ?

        * EditText Interact Check
        > Apakah ada instance EditText yang `isDisplayed()` ? bisa `typeText()` ? bisa `pressKey()` ?

        * Close SoftKeyboard Check
        > Apakah ada instance SoftKeyboard dapat menerima permintaan untuk `closeSoftKeyboard()` ?

        * ViewPager Check
        > Apakah ada instance ViewPager yang `isDisplayed()` ?

        * ViewPager Menu Check [ Movies ]
        > Apakah ada teks yang `isDisplayed()` ? bisa `click()` ?

        <!-- SearchMoviesFragment -->

        * ViewPager Menu Check [ Movies ]
        > Apakah ada teks yang `isDisplayed()` ? bisa `click()` ?

        * Item Visibility & Scroll Check [ Movies ]
        > Apakah item pada indeks ke n `isDisplayed()` ? bisa `scrollToPosition()` ?

        * ViewPager Menu Check [ TV Show ]
        > Apakah ada teks yang `isDisplayed()` ? bisa `click()` ?

        <!-- SearchTVShowFragment -->

        * ViewPager Menu Check [ TV Show ]
        > Apakah ada teks yang `isDisplayed()` ? bisa `click()` ?

        * Item Visibility & Scroll Check [ TV Show ]
        > Apakah item pada indeks ke n `isDisplayed()` ? bisa `scrollToPosition()` ?

    * Fungsi checkFavoriteMoviesPageFragment() [-> `MainActivity/FavoriteMoviesPageFragment`]

        <!-- MainActivity -->

        * BottomNavigationView Menu Check 
        > Apakah menu item pada BottomNavigationMenu `isDisplayed()` ? bisa `click()` ?

        <!-- FavoriteMoviesPageFragment -->

        * RecyclerView Check at NULL / Empty Favorites List
        > Apakah ada RecyclerView yang tidak `isDisplayed()` ?

        * ImageView Check at NULL / Empty Favorites List
        > Apakah ada ImageView yang `isDisplayed()` ?

        * TextView Check at NULL / Empty Favorites List
        > Apakah ada TextView yang `isDisplayed()` ?

    * Fungsi checkFavoriteTVShowsPageFragment() [-> `MainActivity/FavoriteTVShowsPageFragment`]

        <!-- MainActivity -->

        * BottomNavigationView Menu Check 
        > Apakah menu item pada BottomNavigationMenu `isDisplayed()` ? bisa `click()` ?

        <!-- FavoriteTVShowsPageFragment -->

        * RecyclerView Check at NULL / Empty Favorites List
        > Apakah ada RecyclerView yang tidak `isDisplayed()` ?

        * ImageView Check at NULL / Empty Favorites List
        > Apakah ada ImageView yang `isDisplayed()` ?

        * TextView Check at NULL / Empty Favorites List
        > Apakah ada TextView yang `isDisplayed()` ?

* DetailsMovieActivityTest

    * testItems() [-> `MainActivity/SearchPageFragment`]

        <!-- MainActivity -->

        * BottomNavigationView Menu Check 
        > Apakah menu item pada BottomNavigationMenu `isDisplayed()` ? bisa `click()` ?

        <!-- SearchPageFragment -->

        * Toolbar Menu Item Check 
        > Apakah menu item pada Toolbar `isDisplayed()` ? bisa `click()` ?

        * EditText Interact Check
        > Apakah ada instance EditText yang `isDisplayed()` ? bisa `typeText()` ? bisa `pressKey()` ?

        * Close SoftKeyboard Check
        > Apakah ada instance SoftKeyboard dapat menerima permintaan untuk `closeSoftKeyboard()` ?

        * ViewPager Check
        > Apakah ada instance ViewPager yang `isDisplayed()` ?

        * ViewPager Menu Check [ Movies ]
        > Apakah ada teks yang `isDisplayed()` ? bisa `click()` ?

        * testSingleItem()
        > Menguji item pada ViewHolder untuk item pada indeks ke n...sampai..n [ Tanpa Favorites ]

    * testFavorites() [-> `MainActivity/SearchPageFragment` -> `MainActivity/FavoriteMoviesPageFragment`]

        <!-- MainActivity -->

        * BottomNavigationView Menu Check 
        > Apakah menu item pada BottomNavigationMenu `isDisplayed()` ? bisa `click()` ?

        <!-- SearchPageFragment -->

        * Toolbar Menu Item Check 
        > Apakah menu item pada Toolbar `isDisplayed()` ? bisa `click()` ?

        * EditText Interact Check
        > Apakah ada instance EditText yang `isDisplayed()` ? bisa `typeText()` ? bisa `pressKey()` ?

        * Close SoftKeyboard Check
        > Apakah ada instance SoftKeyboard dapat menerima permintaan untuk `closeSoftKeyboard()` ?

        * ViewPager Check
        > Apakah ada instance ViewPager yang `isDisplayed()` ?

        * ViewPager Menu Check [ Movies ]
        > Apakah ada teks yang `isDisplayed()` ? bisa `click()` ?

        * testSingleItem()
        > Menguji item pada ViewHolder untuk item pada indeks ke n...sampai..n [ Dengan Favorites ]

        * testSingleItemFavorite() [ Movies ]
        > Menguji item pada ViewHolder untuk item pada indeks ke n...sampai..n untuk menguji aktivitas paging untuk favorites

        <!-- MainActivity -->

        * BottomNavigationView Menu Check 
        > Apakah menu item pada BottomNavigationMenu `isDisplayed()` ? bisa `click()` ?

        <!-- FavoriteMoviesPageFragment -->

        * RecyclerView Check
        > Apakah ada instance RecyclerView yang `isDisplayed()` ?

        * ImageView Check
        > Apakah ada instance ImageView yang `isDisplayed()` ?

        * TextView Check
        > Apakah ada instance TextView yang `isDisplayed()` ?

    * Fungsi testSingleItem() [-> `MainActivity/SearchPageFragment` -> `DetailsMoviesActivity`  -> `Web Browser` -> `MainActivity/SearchPageFragment`]

        <!-- SearchPageFragment -->

        * ViewPager Menu Check [ Movies ]
        > Apakah ada teks yang `isDisplayed()` ? bisa `click()` ?

        <!-- SearchMovieFragment -->

        * ViewPager Menu Check [ Movies ]
        > Apakah ada teks yang `isDisplayed()` ? bisa `click()` ?

        * Item Visibility & Scroll Check [ Movies ]
        > Apakah item pada indeks ke n `isDisplayed()` ? bisa `scrollToPosition()` ? bisa `click()` ?
        
        * Item Visibility & Scroll Check [ Movies ]
        > Apakah item pada indeks ke n `isDisplayed()` ? bisa `scrollToPosition()` ? bisa `click()` ?

        <!-- DetailsMoviesActivity -->

        * R.id.IVPoster Check
        > Apakah view dengan ID R.id.IVPoster ada dan `isDisplayed()` ?

        * R.id.TVTitle Check
        > Apakah view dengan ID R.id.TVTitle ada dan `isDisplayed()` ?

        * R.id.TVYear Check
        > Apakah view dengan ID R.id.TVYear ada dan `isDisplayed()` ?

        * R.id.TVOverview Check
        > Apakah view dengan ID R.id.TVOverview ada dan `isDisplayed()` ?

        * R.id.TVGenre Check
        > Apakah view dengan ID R.id.TVGenre ada dan `isDisplayed()` ?

        * R.id.TVType Check
        > Apakah view dengan ID R.id.TVType ada dan `isDisplayed()` ?

        * R.id.TVContentRating Check
        > Apakah view dengan ID R.id.TVContentRating ada dan `isDisplayed()` ?

        * R.id.TVDuration Check
        > Apakah view dengan ID R.id.TVDuration ada dan `isDisplayed()` ?

        * R.id.FABFavorites Check
        > Apakah view dengan ID R.id.FABFavorites ada dan `isDisplayed()` ? dan bisa `click()` ? [ Kondisional ]

        * R.id.BSource Check
        > Apakah view dengan ID R.id.BSource ada dan `isDisplayed()` ? dan bisa `click()` ? yang kemudian akan secara otomatis membuka halaman TMDB dari Show terkait menurut properti `sourceLink` pada sebuah Web Browser

        <!-- Web Browser -->

        * Relaunch MainActivity
        > Apakah MainActivity dapat diluncurkan kembali seperti semula setelah keluar dari aplikasi dan meluncurkan Wen Browser ?

        <!-- MainActivity -->

    * Fungsi testSingleItemFavorites() [-> `MainActivity` -> `FavoriteMoviesPageFragment` -> `DetailsMoviesActivity` -> `MainActivity`]

        <!-- SearchPageFragment -->

        * ViewPager Menu Check [ Movies ]
        > Apakah ada teks yang `isDisplayed()` ? bisa `click()` ?

        <!-- SearchMovieFragment -->

        * ViewPager Menu Check [ Movies ]
        > Apakah ada teks yang `isDisplayed()` ? bisa `click()` ?

        * Item Visibility & Scroll Check [ Movies ]
        > Apakah item pada indeks ke n `isDisplayed()` ? bisa `scrollToPosition()` ? bisa `click()` ?
        
        * Item Visibility & Scroll Check [ Movies ]
        > Apakah item pada indeks ke n `isDisplayed()` ? bisa `scrollToPosition()` ? bisa `click()` ?

        <!-- DetailsMoviesActivity -->

        * R.id.IVPoster Check
        > Apakah view dengan ID R.id.IVPoster ada dan `isDisplayed()` ?

        * R.id.TVTitle Check
        > Apakah view dengan ID R.id.TVTitle ada dan `isDisplayed()` ?

        * R.id.TVYear Check
        > Apakah view dengan ID R.id.TVYear ada dan `isDisplayed()` ?

        * R.id.TVOverview Check
        > Apakah view dengan ID R.id.TVOverview ada dan `isDisplayed()` ?

        * R.id.TVGenre Check
        > Apakah view dengan ID R.id.TVGenre ada dan `isDisplayed()` ?

        * R.id.TVType Check
        > Apakah view dengan ID R.id.TVType ada dan `isDisplayed()` ?

        * R.id.TVContentRating Check
        > Apakah view dengan ID R.id.TVContentRating ada dan `isDisplayed()` ?

        * R.id.TVDuration Check
        > Apakah view dengan ID R.id.TVDuration ada dan `isDisplayed()` ?

        * R.id.FABFavorites Check
        > Apakah view dengan ID R.id.FABFavorites ada dan `isDisplayed()` ? dan bisa `click()` ?

        * R.id.BSource Check
        > Apakah view dengan ID R.id.BSource ada dan `isDisplayed()`

        * Relaunch MainActivity
        > Apakah MainActivity dapat diluncurkan kembali ?

        <!-- MainActivity -->
        
* DetailsTVShowActivityTest

    * testItems() [-> `MainActivity/SearchPageFragment`]

        <!-- MainActivity -->

        * BottomNavigationView Menu Check 
        > Apakah menu item pada BottomNavigationMenu `isDisplayed()` ? bisa `click()` ?

        <!-- SearchPageFragment -->

        * Toolbar Menu Item Check 
        > Apakah menu item pada Toolbar `isDisplayed()` ? bisa `click()` ?

        * EditText Interact Check
        > Apakah ada instance EditText yang `isDisplayed()` ? bisa `typeText()` ? bisa `pressKey()` ?

        * Close SoftKeyboard Check
        > Apakah ada instance SoftKeyboard dapat menerima permintaan untuk `closeSoftKeyboard()` ?

        * ViewPager Check
        > Apakah ada instance ViewPager yang `isDisplayed()` ?

        * ViewPager Menu Check [ TV Show ]
        > Apakah ada teks yang `isDisplayed()` ? bisa `click()` ?

        * testSingleItem()
        > Menguji item pada ViewHolder untuk item pada indeks ke n...sampai..n [ Tanpa Favorites ] 

        * testSingleItemFavorite() [ Movies ]
        > Menguji item pada ViewHolder untuk item pada indeks ke n...sampai..n untuk menguji aktivitas paging untuk favorites

        <!-- MainActivity -->

        * BottomNavigationView Menu Check 
        > Apakah menu item pada BottomNavigationMenu `isDisplayed()` ? bisa `click()` ?

        <!-- FavoriteMoviesPageFragment -->

        * RecyclerView Check
        > Apakah ada instance RecyclerView yang `isDisplayed()` ?

        * ImageView Check
        > Apakah ada instance ImageView yang `isDisplayed()` ?

        * TextView Check
        > Apakah ada instance TextView yang `isDisplayed()` ?

    * Fungsi testSingleItem() [-> `MainActivity/SearchPageFragment` -> `DetailsTVShowsActivity`  -> `Web Browser` -> `MainActivity/SearchPageFragment`]

        <!-- SearchPageFragment -->

        * ViewPager Menu Check [ TV Show ]
        > Apakah ada teks yang `isDisplayed()` ? bisa `click()` ?

        <!-- SearchMovieFragment -->

        * ViewPager Menu Check [ TV Show ]
        > Apakah ada teks yang `isDisplayed()` ? bisa `click()` ?

        * Item Visibility & Scroll Check [ TV Show ]
        > Apakah item pada indeks ke n `isDisplayed()` ? bisa `scrollToPosition()` ? bisa `click()` ?
        
        * Item Visibility & Scroll Check [ TV Show ]
        > Apakah item pada indeks ke n `isDisplayed()` ? bisa `scrollToPosition()` ? bisa `click()` ?

        <!-- DetailsTVShowsActivity -->

        * R.id.IVPoster Check
        > Apakah view dengan ID R.id.IVPoster ada dan `isDisplayed()` ?

        * R.id.TVTitle Check
        > Apakah view dengan ID R.id.TVTitle ada dan `isDisplayed()` ?

        * R.id.TVYear Check
        > Apakah view dengan ID R.id.TVYear ada dan `isDisplayed()` ?

        * R.id.TVOverview Check
        > Apakah view dengan ID R.id.TVOverview ada dan `isDisplayed()` ?

        * R.id.TVGenre Check
        > Apakah view dengan ID R.id.TVGenre ada dan `isDisplayed()` ?

        * R.id.TVType Check
        > Apakah view dengan ID R.id.TVType ada dan `isDisplayed()` ?

        * R.id.TVContentRating Check
        > Apakah view dengan ID R.id.TVContentRating ada dan `isDisplayed()` ?

        * R.id.TVDuration Check
        > Apakah view dengan ID R.id.TVDuration ada dan `isDisplayed()` ?

        * R.id.FABFavorites Check
        > Apakah view dengan ID R.id.FABFavorites ada dan `isDisplayed()` ? dan bisa `click()` ? [ Kondisional ]

        * R.id.BSource Check
        > Apakah view dengan ID R.id.BSource ada dan `isDisplayed()` ? dan bisa `click()` ? yang kemudian akan secara otomatis membuka halaman TMDB dari Show terkait menurut properti `sourceLink` pada sebuah Web Browser

        <!-- Web Browser -->

        * Relaunch MainActivity
        > Apakah MainActivity dapat diluncurkan kembali seperti semula setelah keluar dari aplikasi dan meluncurkan Wen Browser ?

        <!-- MainActivity -->

    * Fungsi testSingleItemFavorites() [-> `MainActivity` -> `FavoriteTVShowsPageFragment` -> `DetailsTVShowsActivity` -> `MainActivity`]

        <!-- SearchPageFragment -->

        * ViewPager Menu Check [ TV Shows ]
        > Apakah ada teks yang `isDisplayed()` ? bisa `click()` ?

        <!-- SearchMovieFragment -->

        * ViewPager Menu Check [ TV Shows ]
        > Apakah ada teks yang `isDisplayed()` ? bisa `click()` ?

        * Item Visibility & Scroll Check [ TV Shows ]
        > Apakah item pada indeks ke n `isDisplayed()` ? bisa `scrollToPosition()` ? bisa `click()` ?
        
        * Item Visibility & Scroll Check [ TV Shows ]
        > Apakah item pada indeks ke n `isDisplayed()` ? bisa `scrollToPosition()` ? bisa `click()` ?

        <!-- DetailsTVShowsActivity -->

        * R.id.IVPoster Check
        > Apakah view dengan ID R.id.IVPoster ada dan `isDisplayed()` ?

        * R.id.TVTitle Check
        > Apakah view dengan ID R.id.TVTitle ada dan `isDisplayed()` ?

        * R.id.TVYear Check
        > Apakah view dengan ID R.id.TVYear ada dan `isDisplayed()` ?

        * R.id.TVOverview Check
        > Apakah view dengan ID R.id.TVOverview ada dan `isDisplayed()` ?

        * R.id.TVGenre Check
        > Apakah view dengan ID R.id.TVGenre ada dan `isDisplayed()` ?

        * R.id.TVType Check
        > Apakah view dengan ID R.id.TVType ada dan `isDisplayed()` ?

        * R.id.TVContentRating Check
        > Apakah view dengan ID R.id.TVContentRating ada dan `isDisplayed()` ?

        * R.id.TVDuration Check
        > Apakah view dengan ID R.id.TVDuration ada dan `isDisplayed()` ?

        * R.id.FABFavorites Check
        > Apakah view dengan ID R.id.FABFavorites ada dan `isDisplayed()` ? dan bisa `click()` ?

        * R.id.BSource Check
        > Apakah view dengan ID R.id.BSource ada dan `isDisplayed()`

        * Relaunch MainActivity
        > Apakah MainActivity dapat diluncurkan kembali ?

        <!-- MainActivity -->
        

