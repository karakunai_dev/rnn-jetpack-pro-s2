package rnn.practice.rnnjetpackpros2.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.github.kittinunf.result.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import rnn.practice.rnnjetpackpros2.application.AppDefinition

@ExperimentalCoroutinesApi
class TMDBRepositoryTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    private var checkCounter = 0

    private lateinit var repository: TMDBRepository

    @Before
    fun setUp() {
        repository = TMDBRepository(AppDefinition())
    }

    @After
    fun tearDown() {
        testCoroutineDispatcher.cancel()
        println("# $checkCounter Checks Passed\n")
    }

    @Test
    fun `simulate searchMovies query`() = runBlockingTest {

        repository = mock(TMDBRepository::class.java)

        launch(testCoroutineDispatcher) {

            println("# Currently Asserting -> simulate searchMovies query")

            // Mock searchMovies() method
            `when`(repository.searchMovies("Case Closed")).thenReturn(Result.of { dummyStringReturn() })

            println("> searchMovies() Method Execution Check")
            // Asserts whether the return string of searchMovies() method is equal to "Case Closed"
            assertEquals(Result.success("Case Closed"), repository.searchMovies("Case Closed"))
            checkCounter++
        }

        testCoroutineDispatcher.cancel()
    }

    @Test
    fun `simulate searchTVShows query`() = runBlockingTest {

        repository = mock(TMDBRepository::class.java)

        launch(testCoroutineDispatcher) {

            println("# Currently Asserting -> simulate searchTVShows query")

            // Mock searchTVShows() method
            `when`(repository.searchTVShows("Case Closed")).thenReturn(Result.of { dummyStringReturn() })

            println("> searchTVShows() Method Execution Check")
            // Asserts whether the return string of searchTVShows() method is equal to "Case Closed"
            assertEquals(Result.success("Case Closed"), repository.searchTVShows("Case Closed"))
            checkCounter++
        }

        testCoroutineDispatcher.cancel()
    }

    @Test
    fun `simulate detailsMovie query`() = runBlockingTest {

        repository = mock(TMDBRepository::class.java)

        launch(testCoroutineDispatcher) {

            println("# Currently Asserting -> simulate detailsMovie query")

            // Mock detailsMovie() method
            `when`(repository.detailsMovie("Case Closed")).thenReturn(Result.of { dummyStringReturn() })

            println("> detailsMovie() Method Execution Check")
            // Asserts whether the return string of detailsMovie() method is equal to "Case Closed"
            assertEquals(Result.success("Case Closed"), repository.detailsMovie("Case Closed"))
            checkCounter++
        }

        testCoroutineDispatcher.cancel()
    }

    @Test
    fun `simulate detailsTVShow query`() = runBlockingTest {

        repository = mock(TMDBRepository::class.java)

        launch(testCoroutineDispatcher) {

            println("# Currently Asserting -> simulate detailsTVShow query")

            // Mock detailsTVShow() method
            `when`(repository.detailsTVShow("Case Closed")).thenReturn(Result.of { dummyStringReturn() })

            println("> detailsTVShow() Method Execution Check")
            // Asserts whether the return string of detailsTVShow() method is equal to "Case Closed"
            assertEquals(Result.success("Case Closed"), repository.detailsTVShow("Case Closed"))
            checkCounter++
        }

        testCoroutineDispatcher.cancel()
    }

    private fun dummyStringReturn(): String = "Case Closed"
}