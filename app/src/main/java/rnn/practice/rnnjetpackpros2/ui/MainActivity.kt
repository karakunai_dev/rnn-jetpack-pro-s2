package rnn.practice.rnnjetpackpros2.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import rnn.practice.rnnjetpackpros2.R
import rnn.practice.rnnjetpackpros2.databinding.ActivityMainBinding
import rnn.practice.rnnjetpackpros2.fragments.FavoriteMoviesPageFragment
import rnn.practice.rnnjetpackpros2.fragments.FavoriteTVShowsPageFragment
import rnn.practice.rnnjetpackpros2.fragments.SearchPageFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.hide()

        savedInstanceState ?: setPageFragment(SearchPageFragment())

        binding.BNVMainActivity.apply {
            setOnNavigationItemSelectedListener {
                when(it.itemId) {
                    R.id.MISearch -> {
                        setPageFragment(SearchPageFragment())
                        true
                    }
                    R.id.MIFavoritesM -> {
                        setPageFragment(FavoriteMoviesPageFragment())
                        true
                    }
                    R.id.MIFavoritesT -> {
                        setPageFragment(FavoriteTVShowsPageFragment())
                        true
                    }
                    else -> false
                }
            }
        }
    }

    private fun setPageFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.FLMainActivity, fragment)
            .commit()
    }
}