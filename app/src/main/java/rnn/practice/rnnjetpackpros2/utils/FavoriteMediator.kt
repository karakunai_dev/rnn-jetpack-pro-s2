package rnn.practice.rnnjetpackpros2.utils

import android.content.Context
import android.util.Log
import androidx.lifecycle.asLiveData
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.kodein.di.DI
import org.kodein.di.DIAware
import org.kodein.di.android.closestDI
import rnn.practice.rnnjetpackpros2.database.FavoriteTMDBShowDB
import rnn.practice.rnnjetpackpros2.models.roomdb.FavoriteRemoteKey
import rnn.practice.rnnjetpackpros2.models.roomdb.FavoriteTMDBShow
import java.io.IOException
import java.io.InvalidObjectException

@ExperimentalPagingApi
class FavoriteMediator(private val db: FavoriteTMDBShowDB) : RemoteMediator<Int, FavoriteTMDBShow>() {

    override suspend fun load(loadType: LoadType, state: PagingState<Int, FavoriteTMDBShow>): MediatorResult {

        var currentKeyIndex: Int

        with(getIndexKeyByState(loadType, state)) {
            currentKeyIndex = when(loadType) {
                LoadType.REFRESH -> {
                    this as Int
                }
                LoadType.APPEND -> {
                    this ?: throw InvalidObjectException("Key is null, state $loadType")
                }
                LoadType.PREPEND -> {
                    this ?: throw InvalidObjectException("Key is null, state $loadType")
                }
            }
        }

        var currentMediatorResult : MediatorResult

        withContext(Dispatchers.IO) {
            try {

                val collection = db.pagingFavoriteShowDao().getShows()

                db.withTransaction {
                    if (loadType == LoadType.REFRESH) {
                        db.pagingFavoriteKeyDao().pagingResetKeys()
                    }

                    val mappedKeys = collection?.map { FavoriteRemoteKey(it.intId, if (currentKeyIndex == 1) null else (currentKeyIndex - 1), if (collection.isEmpty()) null else (currentKeyIndex + 1) ) } ?: listOf(FavoriteRemoteKey(0, 0, 0))

                    db.pagingFavoriteKeyDao().pagingInsertKeys(mappedKeys)
                }

                currentMediatorResult = MediatorResult.Success(endOfPaginationReached = (collection?.isEmpty() == true))
            } catch (err: IOException) {
                currentMediatorResult = MediatorResult.Error(err)
            }
        }

        return currentMediatorResult
    }

    suspend fun getIndexKeyByState(loadType: LoadType, currentState: PagingState<Int, FavoriteTMDBShow>) : Int? {
        return when(loadType) {
            LoadType.REFRESH -> {
                getIndexKeyByNeighbor(currentState)?.nextKeyId?.minus(1) ?: 1
            }
            LoadType.APPEND -> {
                with(getIndexKeyByOrder(false, currentState)) {
                    this?.nextKeyId ?: 1
                }
            }
            LoadType.PREPEND -> {
                with(getIndexKeyByOrder(true, currentState)) {
                    this?.previousKeyId ?: 1
                }
            }
        }
    }

    private suspend fun getIndexKeyByOrder(isFirst: Boolean, currentState: PagingState<Int, FavoriteTMDBShow>) : FavoriteRemoteKey? {
        return if (isFirst) currentState.pages.firstOrNull { it.data.isNotEmpty() }?.data?.firstOrNull()?.let { item -> db.pagingFavoriteKeyDao().pagingGetKey(item.intId) } else currentState.pages.lastOrNull { it.data.isNotEmpty() }?.data?.lastOrNull()?.let { item -> db.pagingFavoriteKeyDao().pagingGetKey(item.intId) }
    }

    private suspend fun getIndexKeyByNeighbor(currentState: PagingState<Int, FavoriteTMDBShow>) : FavoriteRemoteKey? {
        return currentState.anchorPosition?.let { index -> currentState.closestItemToPosition(index)?.intId?.let { id -> db.pagingFavoriteKeyDao().pagingGetKey(id) } }
    }
}