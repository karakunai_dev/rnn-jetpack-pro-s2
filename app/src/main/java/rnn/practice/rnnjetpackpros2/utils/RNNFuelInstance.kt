package rnn.practice.rnnjetpackpros2.utils

import com.github.kittinunf.fuel.core.FuelManager

object RNNFuelInstance {
    internal fun init() : FuelManager {
        return FuelManager()
    }
}