package rnn.practice.rnnjetpackpros2.utils

import androidx.test.espresso.idling.CountingIdlingResource

object IdlingResourceInstance {

    private const val RESOURCE = "GLOBAL"

    @JvmField
    val currentResource = CountingIdlingResource(RESOURCE)

    fun incrementCounter() {
        currentResource.increment()
    }

    fun decrementCounter() {
        if (!currentResource.isIdleNow) {
            currentResource.decrement()
        }
    }
}