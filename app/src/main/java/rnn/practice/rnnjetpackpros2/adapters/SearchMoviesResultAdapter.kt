package rnn.practice.rnnjetpackpros2.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import rnn.practice.rnnjetpackpros2.R
import rnn.practice.rnnjetpackpros2.databinding.ItemSearchResultBinding
import rnn.practice.rnnjetpackpros2.models.tmdb.search.movie.SearchMovieItem

class SearchMoviesResultAdapter(private val items: ArrayList<SearchMovieItem>, private val listener: (SearchMovieItem) -> Unit) : RecyclerView.Adapter<SearchMoviesResultAdapter.SearchMoviesResultViewHolder>() {

    companion object {
        private const val TMDB_BASE_IMAGE_PATH = "https://image.tmdb.org/t/p/w92"
    }

    inner class SearchMoviesResultViewHolder(private val binding: ItemSearchResultBinding) : RecyclerView.ViewHolder(binding.root) {
        fun getBinding() = binding
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchMoviesResultViewHolder {
        return SearchMoviesResultViewHolder(ItemSearchResultBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: SearchMoviesResultViewHolder, position: Int) {
        holder.apply {
            getBinding().apply {
                items[position].also { current ->
                    itemView.setOnClickListener { listener(current) }
                    TVTitle.text = current.stringTitle
                    TVOverview.text = current.stringOverview
                    TVYear.text = current.stringReleaseDate
                    Glide.with(this.root)
                        .load("$TMDB_BASE_IMAGE_PATH${current.stringPoster}")
                        .placeholder(R.drawable.ic_baseline_image_search_24)
                        .error(R.drawable.ic_baseline_broken_image_24)
                        .into(IVPoster)
                }
            }
        }
    }

    override fun getItemCount(): Int = items.size
}