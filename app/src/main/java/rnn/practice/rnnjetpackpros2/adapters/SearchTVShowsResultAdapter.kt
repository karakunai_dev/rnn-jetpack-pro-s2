package rnn.practice.rnnjetpackpros2.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import rnn.practice.rnnjetpackpros2.R
import rnn.practice.rnnjetpackpros2.databinding.ItemSearchResultBinding
import rnn.practice.rnnjetpackpros2.models.tmdb.search.tvshows.SearchTVShowsItem

class SearchTVShowsResultAdapter(private val items: ArrayList<SearchTVShowsItem>, private val listener: (SearchTVShowsItem) -> Unit) : RecyclerView.Adapter<SearchTVShowsResultAdapter.SearchTVShowsResultViewHolder>() {

    companion object {
        private const val TMDB_BASE_IMAGE_PATH = "https://image.tmdb.org/t/p/w92"
    }

    inner class SearchTVShowsResultViewHolder(private val binding: ItemSearchResultBinding) : RecyclerView.ViewHolder(binding.root) {
        fun getBinding() = binding
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchTVShowsResultViewHolder {
        return SearchTVShowsResultViewHolder(ItemSearchResultBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: SearchTVShowsResultViewHolder, position: Int) {
        holder.apply {
            getBinding().apply {
                items[position].also { current ->
                    itemView.setOnClickListener { listener(current) }
                        TVTitle.text = current.stringTitle
                        TVOverview.text = current.stringOverview
                        TVYear.text = current.stringFirstAirDate
                        Glide.with(this.root)
                            .load("$TMDB_BASE_IMAGE_PATH${current.stringPoster}")
                            .placeholder(R.drawable.ic_baseline_image_search_24)
                            .error(R.drawable.ic_baseline_broken_image_24)
                            .into(IVPoster)
                }
            }
        }
    }

    override fun getItemCount(): Int = items.size
}