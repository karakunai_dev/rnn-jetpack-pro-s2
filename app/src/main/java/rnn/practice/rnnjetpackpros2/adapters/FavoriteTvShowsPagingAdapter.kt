package rnn.practice.rnnjetpackpros2.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import rnn.practice.rnnjetpackpros2.R
import rnn.practice.rnnjetpackpros2.databinding.ItemSearchResultBinding
import rnn.practice.rnnjetpackpros2.models.roomdb.FavoriteTMDBShow

class FavoriteTvShowsPagingAdapter(private val listener: (FavoriteTMDBShow) -> Unit) : PagingDataAdapter<FavoriteTMDBShow, FavoriteTvShowsPagingAdapter.FavoriteTvShowsPagingViewHolder>(FAVORITE_COMPARATOR) {

    companion object {

        private const val TMDB_BASE_IMAGE_PATH = "https://image.tmdb.org/t/p/w92"

        private val FAVORITE_COMPARATOR = object : DiffUtil.ItemCallback<FavoriteTMDBShow>() {
            override fun areContentsTheSame(oldItem: FavoriteTMDBShow, newItem: FavoriteTMDBShow): Boolean = oldItem == newItem

            override fun areItemsTheSame(oldItem: FavoriteTMDBShow, newItem: FavoriteTMDBShow): Boolean = oldItem.intId == newItem.intId
        }
    }

    inner class FavoriteTvShowsPagingViewHolder(private val binding: ItemSearchResultBinding) : RecyclerView.ViewHolder(binding.root) {
        fun getBinding() = binding
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteTvShowsPagingViewHolder {
        return FavoriteTvShowsPagingViewHolder(ItemSearchResultBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: FavoriteTvShowsPagingViewHolder, position: Int) {
        holder.apply {
            getBinding().apply {
                getItem(position).also { current ->
                    if (current != null) {
                        itemView.setOnClickListener { listener(current) }
                        TVTitle.text = current.stringTitle
                        TVOverview.text = current.stringOverview
                        TVYear.text = current.stringDate
                        Glide.with(this.root)
                                .load("${TMDB_BASE_IMAGE_PATH}${current.stringPoster}")
                                .placeholder(R.drawable.ic_baseline_image_search_24)
                                .error(R.drawable.ic_baseline_broken_image_24)
                                .into(IVPoster)
                    }
                }
            }
        }
    }
}