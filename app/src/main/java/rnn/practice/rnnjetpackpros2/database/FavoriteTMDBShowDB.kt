package rnn.practice.rnnjetpackpros2.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import rnn.practice.rnnjetpackpros2.interfaces.IFavoriteRemoteKeyDao
import rnn.practice.rnnjetpackpros2.interfaces.IFavoriteShowDao
import rnn.practice.rnnjetpackpros2.interfaces.IPagingFavoriteShowDao
import rnn.practice.rnnjetpackpros2.models.roomdb.FavoriteRemoteKey
import rnn.practice.rnnjetpackpros2.models.roomdb.FavoriteTMDBShow

@Database(entities = [FavoriteTMDBShow::class, FavoriteRemoteKey::class], version = 2, exportSchema = false)
abstract class FavoriteTMDBShowDB : RoomDatabase() {

    abstract fun favoriteDao() : IFavoriteShowDao
    abstract fun pagingFavoriteShowDao() : IPagingFavoriteShowDao
    abstract fun pagingFavoriteKeyDao() : IFavoriteRemoteKeyDao

    companion object {
        const val DATABASE_NAME = "rnn_db_room_favorites"

        @Volatile
        private var CURRENT_INSTANCE: FavoriteTMDBShowDB? = null

        fun initDatabase(context: Context) : FavoriteTMDBShowDB {
            return CURRENT_INSTANCE ?: synchronized(this) {
                val newInstance = Room.databaseBuilder(
                    context,
                    FavoriteTMDBShowDB::class.java,
                    DATABASE_NAME
                ).build()

                CURRENT_INSTANCE = newInstance

                newInstance
            }
        }
    }
}