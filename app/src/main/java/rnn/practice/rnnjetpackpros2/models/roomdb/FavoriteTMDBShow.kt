package rnn.practice.rnnjetpackpros2.models.roomdb

import android.provider.BaseColumns
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = FavoriteTMDBShow.TABLE_NAME)
data class FavoriteTMDBShow (

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = TABLE_COLUMN_PRIMARY, index = true)
    var keyIndex: Int = 0,

    @ColumnInfo(name = TABLE_COLUMN_TITLE)
    var stringTitle: String? = null,

    @ColumnInfo(name = TABLE_COLUMN_OVERVIEW)
    var stringOverview: String? = null,

    @ColumnInfo(name = TABLE_COLUMN_DATE)
    var stringDate: String? = "yyyy-mm-dd",

    @ColumnInfo(name = TABLE_COLUMN_POSTER)
    var stringPoster: String? = null,

    @ColumnInfo(name = TABLE_COLUMN_TYPE)
    var isMovie: Boolean = true,

    @ColumnInfo(name = TABLE_COLUMN_ID)
    var intId: Int = 0

) {
    companion object {
        const val TABLE_NAME = "table_favorite"
        const val TABLE_COLUMN_PRIMARY = BaseColumns._ID
        const val TABLE_COLUMN_TITLE = "title"
        const val TABLE_COLUMN_OVERVIEW = "overview"
        const val TABLE_COLUMN_DATE = "date"
        const val TABLE_COLUMN_POSTER = "poster_url"
        const val TABLE_COLUMN_TYPE = "show_type"
        const val TABLE_COLUMN_ID = "show_id"
    }
}
