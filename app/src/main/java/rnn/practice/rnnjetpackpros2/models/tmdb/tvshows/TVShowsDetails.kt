package rnn.practice.rnnjetpackpros2.models.tmdb.tvshows

import com.google.gson.annotations.SerializedName
import rnn.practice.rnnjetpackpros2.models.tmdb.genres.ShowGenre
import rnn.practice.rnnjetpackpros2.models.tmdb.tvshows.contentratings.TVShowsContentRatingsResult

data class TVShowsDetails(
    @SerializedName("id")
    var intId: Int = 0,

    @SerializedName("name")
    var stringTitle: String? = null,

    @SerializedName("first_air_date")
    var stringFirstAirDate: String? = "yyyy-mm-dd",

    @SerializedName("overview")
    var stringOverview: String? = null,

    @SerializedName("poster_path")
    var stringPoster: String? = null,

    @SerializedName("genres")
    var arrayGenres: ArrayList<ShowGenre>,

    @SerializedName("content_ratings")
    var objectTVShowsContentRatingsResult: TVShowsContentRatingsResult,

    @SerializedName("episode_run_time")
    var episodeDuration: ArrayList<Int>? = null,

    @SerializedName("number_of_episodes")
    var episodeCount: Int = 0
)
