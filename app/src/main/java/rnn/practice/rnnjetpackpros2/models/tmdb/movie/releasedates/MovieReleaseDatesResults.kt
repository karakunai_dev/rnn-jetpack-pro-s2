package rnn.practice.rnnjetpackpros2.models.tmdb.movie.releasedates

import com.google.gson.annotations.SerializedName

data class MovieReleaseDatesResults(
    @SerializedName("results")
    var arrayReleaseDates: ArrayList<MovieReleaseDate>
)
