package rnn.practice.rnnjetpackpros2.models.tmdb.search.tvshows

import com.google.gson.annotations.SerializedName

data class SearchTVShowsItem(
    @SerializedName("id")
    var intId: Int = 0,

    @SerializedName("name")
    var stringTitle: String? = null,

    @SerializedName("first_air_date")
    var stringFirstAirDate: String? = "yyyy-mm-dd",

    @SerializedName("overview")
    var stringOverview: String? = null,

    @SerializedName("poster_path")
    var stringPoster: String? = null,
)
