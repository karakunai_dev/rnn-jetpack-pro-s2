package rnn.practice.rnnjetpackpros2.models.tmdb.movie

import com.google.gson.annotations.SerializedName
import rnn.practice.rnnjetpackpros2.models.tmdb.genres.ShowGenre
import rnn.practice.rnnjetpackpros2.models.tmdb.movie.releasedates.MovieReleaseDatesResults

data class MovieDetails(
    @SerializedName("id")
    var intId: Int = 0,

    @SerializedName("title")
    var stringTitle: String? = null,

    @SerializedName("release_date")
    var stringReleaseDate: String? = "yyyy-mm-dd",

    @SerializedName("overview")
    var stringOverview: String? = null,

    @SerializedName("poster_path")
    var stringPoster: String? = null,

    @SerializedName("genres")
    var arrayGenres: ArrayList<ShowGenre>,

    @SerializedName("release_dates")
    var objectReleaseDatesResults: MovieReleaseDatesResults,

    @SerializedName("runtime")
    var duration: Int = 0
)
