package rnn.practice.rnnjetpackpros2.models.tmdb.tvshows.contentratings

import com.google.gson.annotations.SerializedName

data class TVShowsContentRating(
    @SerializedName("iso_3166_1")
    var stringCountry: String? = null,

    @SerializedName("rating")
    var stringRating: String? = null
)
