package rnn.practice.rnnjetpackpros2.models.tmdb.tvshows.contentratings

import com.google.gson.annotations.SerializedName

data class TVShowsContentRatingsResult(
    @SerializedName("results")
    var arrayContentRating: ArrayList<TVShowsContentRating>
)
