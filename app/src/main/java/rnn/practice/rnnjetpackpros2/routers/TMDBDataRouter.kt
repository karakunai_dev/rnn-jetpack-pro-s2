package rnn.practice.rnnjetpackpros2.routers

import com.github.kittinunf.fuel.core.HeaderValues
import com.github.kittinunf.fuel.core.Headers
import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.core.Parameters
import com.github.kittinunf.fuel.util.FuelRouting
import rnn.practice.rnnjetpackpros2.BuildConfig

sealed class TMDBDataRouter : FuelRouting {

    override val basePath: String
        get() {
            return "https://api.themoviedb.org/3"
        }

    class SearchMovie(val queryString: String) : TMDBDataRouter()
    class SearchTVShows(val queryString: String) : TMDBDataRouter()
    class DetailsMovie(val idString: String) : TMDBDataRouter()
    class DetailsTVShows(val idString: String) : TMDBDataRouter()

    override val method: Method
        get() {
            return Method.GET
        }

    override val params: Parameters?
        get() {
            return when(this) {
                is SearchMovie -> listOf("api_key" to BuildConfig.RNN_TMDB_TOKEN, "query" to this.queryString, "region" to "us")
                is SearchTVShows -> listOf("api_key" to BuildConfig.RNN_TMDB_TOKEN, "query" to this.queryString)
                is DetailsMovie -> listOf("api_key" to BuildConfig.RNN_TMDB_TOKEN, "append_to_response" to "release_dates")
                is DetailsTVShows -> listOf("api_key" to BuildConfig.RNN_TMDB_TOKEN, "append_to_response" to "content_ratings")
            }
        }

    override val body: String?
        get() {
            return null
        }

    override val path: String
        get() {
            return when(this) {
                is SearchMovie -> "/search/movie"
                is SearchTVShows -> "/search/tv"
                is DetailsMovie -> "/movie/${this.idString}"
                is DetailsTVShows -> "/tv/${this.idString}"
            }
        }

    override val headers: Map<String, HeaderValues>?
        get() {
            return Headers()
                    .set(Headers.CONTENT_TYPE, "application/json")
        }

    override val bytes: ByteArray?
        get() {
            return null
        }
}
