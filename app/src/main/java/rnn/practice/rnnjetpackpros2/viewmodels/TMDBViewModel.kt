package rnn.practice.rnnjetpackpros2.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import org.kodein.di.DI
import org.kodein.di.DIAware
import org.kodein.di.android.closestDI
import org.kodein.di.instance
import rnn.practice.rnnjetpackpros2.models.roomdb.FavoriteTMDBShow
import rnn.practice.rnnjetpackpros2.models.tmdb.movie.MovieDetails
import rnn.practice.rnnjetpackpros2.models.tmdb.search.movie.SearchMovie
import rnn.practice.rnnjetpackpros2.models.tmdb.search.tvshows.SearchTVShows
import rnn.practice.rnnjetpackpros2.models.tmdb.tvshows.TVShowsDetails
import rnn.practice.rnnjetpackpros2.repository.RoomDBRepository
import rnn.practice.rnnjetpackpros2.repository.TMDBRepository
import rnn.practice.rnnjetpackpros2.utils.RNNState

class TMDBViewModel(app: Application) : AndroidViewModel(app), DIAware {

    override val di: DI by closestDI(app)

    private val tmdbRepository: TMDBRepository by instance()
    private val roomdbRepository: RoomDBRepository by instance()

    private val currentMovie = MutableLiveData<MovieDetails>()
    private val currentMovies = MutableLiveData<SearchMovie>()
    private val currentTVShow = MutableLiveData<TVShowsDetails>()
    private val currentTVShows = MutableLiveData<SearchTVShows>()
    private val currentState = MutableLiveData<RNNState>()
    private val currentFavorite = MutableLiveData<Int>()

    private var currentPagingFavoriteQuery: Int? = null
    private var currentPagingFavorites: Flow<PagingData<FavoriteTMDBShow>>? = null

    val favorites = roomdbRepository.favorites

    fun getMovie() : LiveData<MovieDetails> = currentMovie
    fun getMovies() : LiveData<SearchMovie> = currentMovies
    fun getTVShow() : LiveData<TVShowsDetails> = currentTVShow
    fun getTVShows() : LiveData<SearchTVShows> = currentTVShows
    fun getState() : LiveData<RNNState> = currentState
    fun getFavorite() : LiveData<Int> = currentFavorite


    fun setState(state: RNNState) = currentState.postValue(state)

    fun setState(state: RNNState, listener: (RNNState?) -> Unit) {
        listener(state)
        currentState.postValue(state)
    }

    fun queryMovie(idString: String) : Boolean = viewModelScope.launch(Dispatchers.IO) {

        tmdbRepository.detailsMovie(idString).apply {
            this.fold(
                {
                    currentMovie.postValue(Gson().fromJson(it, MovieDetails::class.java))
                },
                {

                }
            )
        }
    }.isCompleted

    fun queryMovies(queryString: String, listener: (RNNState?) -> Unit) : Boolean = viewModelScope.launch(Dispatchers.IO) {

        setState(RNNState.LOAD, listener)

        tmdbRepository.searchMovies(queryString).apply {
            this.fold(
                {
                    setState(RNNState.DONE, listener)
                    currentMovies.postValue(Gson().fromJson(it, SearchMovie::class.java))
                },
                {
                    setState(RNNState.FAIL, listener)
                }
            )
        }
    }.isCompleted

    fun queryMovies(queryString: String) : Boolean = viewModelScope.launch(Dispatchers.IO) {
        tmdbRepository.searchMovies(queryString).apply {
            this.fold(
                {
                    currentMovies.postValue(Gson().fromJson(it, SearchMovie::class.java))
                },
                {

                }
            )
        }
    }.isCompleted

    fun queryTVShow(idString: String) : Boolean = viewModelScope.launch(Dispatchers.IO) {
        tmdbRepository.detailsTVShow(idString).apply {
            this.fold(
                {
                    currentTVShow.postValue(Gson().fromJson(it, TVShowsDetails::class.java))
                },
                {

                }
            )
        }
    }.isCompleted

    fun queryTVShows(queryString: String, listener: (RNNState?) -> Unit) : Boolean = viewModelScope.launch(Dispatchers.IO) {

        setState(RNNState.LOAD, listener)

        tmdbRepository.searchTVShows(queryString).apply {
            this.fold(
                {
                    setState(RNNState.DONE, listener)
                    currentTVShows.postValue(Gson().fromJson(it, SearchTVShows::class.java))
                },
                {
                    setState(RNNState.FAIL, listener)
                }
            )
        }
    }.isCompleted

    fun queryTVShows(queryString: String) : Boolean = viewModelScope.launch(Dispatchers.IO) {
        tmdbRepository.searchTVShows(queryString).apply {
            this.fold(
                {
                    currentTVShows.postValue(Gson().fromJson(it, SearchTVShows::class.java))
                },
                {

                }
            )
        }
    }.isCompleted

    fun querySearch(queryString: String,listener: (RNNState?) -> Unit) {
        queryMovies(queryString, listener)
        queryTVShows(queryString, listener)
    }

    fun addFavorite(item: FavoriteTMDBShow) : Boolean = viewModelScope.launch(Dispatchers.IO) {
        roomdbRepository.addFavorite(item)
    }.isCompleted

    fun findFavorite(itemID: Int) : Boolean = viewModelScope.launch(Dispatchers.IO) {
        currentFavorite.postValue(roomdbRepository.findFavorite(itemID)?.keyIndex)
    }.isCompleted

    fun removeFavorite(item: FavoriteTMDBShow) : Boolean = viewModelScope.launch(Dispatchers.IO) {
        roomdbRepository.deleteFavorite(item)
    }.isCompleted

    fun resetFavorite() : Boolean = viewModelScope.launch(Dispatchers.IO) {
        roomdbRepository.resetFavorite()
    }.isCompleted

    @ExperimentalPagingApi
    fun pagingFavorite() : Flow<PagingData<FavoriteTMDBShow>> {
        return roomdbRepository.favoritePagingFlow().cachedIn(viewModelScope)
    }
}