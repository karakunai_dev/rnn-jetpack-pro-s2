package rnn.practice.rnnjetpackpros2.interfaces

import androidx.paging.PagingSource
import androidx.room.*
import kotlinx.coroutines.flow.Flow
import rnn.practice.rnnjetpackpros2.models.roomdb.FavoriteTMDBShow

@Dao
interface IFavoriteShowDao {

    @Query("SELECT * FROM table_favorite")
    fun getFavorites(): Flow<List<FavoriteTMDBShow>>

    @Query("SELECT * FROM table_favorite WHERE show_id = :itemId")
    suspend fun getFavorite(itemId: Int) : FavoriteTMDBShow?

    @Query("DELETE FROM table_favorite")
    suspend fun resetFavorite()

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addFavorite(item: FavoriteTMDBShow)

    @Delete
    suspend fun removeFavorite(item: FavoriteTMDBShow)
}