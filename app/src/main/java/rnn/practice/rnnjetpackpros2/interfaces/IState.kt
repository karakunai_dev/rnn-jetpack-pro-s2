package rnn.practice.rnnjetpackpros2.interfaces

import android.util.Log
import rnn.practice.rnnjetpackpros2.utils.RNNState

interface IState {
    fun toggleState(state: RNNState?, className: String) {
        Log.d("RNNState", "state is $state from $className")
    }
}