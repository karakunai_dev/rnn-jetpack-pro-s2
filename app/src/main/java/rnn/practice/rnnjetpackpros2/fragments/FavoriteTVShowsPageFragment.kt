package rnn.practice.rnnjetpackpros2.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.paging.ExperimentalPagingApi
import androidx.paging.filter
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.kodein.di.DI
import org.kodein.di.DIAware
import org.kodein.di.android.x.closestDI
import org.kodein.di.instance
import rnn.practice.rnnjetpackpros2.adapters.FavoriteMoviesPagingAdapter
import rnn.practice.rnnjetpackpros2.adapters.FavoriteTvShowsPagingAdapter
import rnn.practice.rnnjetpackpros2.databinding.FragmentFavoriteTVShowsPageBinding
import rnn.practice.rnnjetpackpros2.ui.DetailsMoviesActivity
import rnn.practice.rnnjetpackpros2.ui.DetailsTVShowActivity
import rnn.practice.rnnjetpackpros2.viewmodels.TMDBViewModel

class FavoriteTVShowsPageFragment : Fragment(), DIAware {

    override val di: DI by closestDI()

    private val viewModel: TMDBViewModel by instance()

    private lateinit var binding: FragmentFavoriteTVShowsPageBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentFavoriteTVShowsPageBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    @ExperimentalPagingApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val itemAdapter = FavoriteTvShowsPagingAdapter() {
            viewModel.queryTVShow(it.intId.toString())
            startActivity(Intent(context, DetailsTVShowActivity::class.java))
        }

        binding.apply {
            TFragment.title = "My TV Shows"
            RVItem.apply {
                layoutManager = LinearLayoutManager(context)
                setHasFixedSize(true)
                adapter = itemAdapter
            }
        }

        viewModel.favorites.observe(this, {
            lifecycleScope.launch {
                viewModel.pagingFavorite().collectLatest { res ->
                    itemAdapter.submitData(res.filter { !it.isMovie })
                }
            }

            binding.apply {
                if (!it.filter { !it.isMovie }.isNullOrEmpty()) {
                    RVItem.visibility = View.VISIBLE
                    LLStateContainer.visibility = View.INVISIBLE
                    IVStateIcon.visibility = View.INVISIBLE
                    TVStateText.visibility = View.INVISIBLE
                } else {
                    RVItem.visibility = View.INVISIBLE
                    LLStateContainer.visibility = View.VISIBLE
                    IVStateIcon.visibility = View.VISIBLE
                    TVStateText.visibility = View.VISIBLE
                }
            }
        })
    }
}