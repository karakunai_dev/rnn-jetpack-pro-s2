package rnn.practice.rnnjetpackpros2.repository

import android.content.Context
import androidx.annotation.WorkerThread
import androidx.lifecycle.asLiveData
import androidx.paging.*
import kotlinx.coroutines.flow.Flow
import org.kodein.di.DI
import org.kodein.di.DIAware
import org.kodein.di.android.closestDI
import org.kodein.di.instance
import rnn.practice.rnnjetpackpros2.database.FavoriteTMDBShowDB
import rnn.practice.rnnjetpackpros2.interfaces.IFavoriteShowDao
import rnn.practice.rnnjetpackpros2.models.roomdb.FavoriteTMDBShow
import rnn.practice.rnnjetpackpros2.utils.FavoriteMediator
import java.io.IOException

class RoomDBRepository(context: Context, private val db: FavoriteTMDBShowDB) : DIAware {

    override val di: DI by closestDI(context)

    private val favoriteDao: IFavoriteShowDao = db.favoriteDao()

    val favorites = favoriteDao.getFavorites().asLiveData()

    @WorkerThread
    suspend fun addFavorite(item: FavoriteTMDBShow) : Boolean {
        favoriteDao.addFavorite(item)
        return true
    }

    @WorkerThread
    suspend fun findFavorite(itemId: Int) : FavoriteTMDBShow? {
        return favoriteDao.getFavorite(itemId)
    }

    @WorkerThread
    suspend fun deleteFavorite(item: FavoriteTMDBShow) : Boolean {
        favoriteDao.removeFavorite(item)
        return true
    }

    @WorkerThread
    suspend fun resetFavorite() : Boolean {
        favoriteDao.resetFavorite()
        return true
    }

    @ExperimentalPagingApi
    @WorkerThread
    fun favoritePagingFlow(config: PagingConfig = PagingConfig(pageSize = 1, enablePlaceholders = true)) : Flow<PagingData<FavoriteTMDBShow>> {
        return Pager(config = config, pagingSourceFactory = { db.pagingFavoriteShowDao().pagingGetShows() }, remoteMediator = FavoriteMediator(db)).flow
    }

    @ExperimentalStdlibApi
    @ExperimentalPagingApi
    @WorkerThread
    fun favoritePagingFlowFake(config: PagingConfig = PagingConfig(pageSize = 1, enablePlaceholders = true)) : Boolean {
        return try {
            Pager(config = config, pagingSourceFactory = { db.pagingFavoriteShowDao().pagingGetShows() }, remoteMediator = FavoriteMediator(db)).flow

            true
        } catch (err: IOException) {
            false
        }
    }
}