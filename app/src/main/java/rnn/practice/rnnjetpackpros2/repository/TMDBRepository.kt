package rnn.practice.rnnjetpackpros2.repository

import android.content.Context
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.coroutines.awaitStringResult
import com.github.kittinunf.result.Result
import org.kodein.di.DI
import org.kodein.di.DIAware
import org.kodein.di.android.closestDI
import org.kodein.di.instance
import rnn.practice.rnnjetpackpros2.interfaces.IServiceTMDB
import rnn.practice.rnnjetpackpros2.routers.TMDBDataRouter

class TMDBRepository(context: Context) : IServiceTMDB, DIAware {

    override val di: DI by closestDI(context)

    private val fuelInstance: FuelManager by instance()

    override suspend fun searchMovies(titleString: String): Result<String, FuelError> {
        return fuelInstance.request(TMDBDataRouter.SearchMovie(titleString)).awaitStringResult()
    }

    override suspend fun searchTVShows(titleString: String): Result<String, FuelError> {
        return fuelInstance.request(TMDBDataRouter.SearchTVShows(titleString)).awaitStringResult()
    }

    override suspend fun detailsMovie(idString: String): Result<String, FuelError> {
        return fuelInstance.request(TMDBDataRouter.DetailsMovie(idString)).awaitStringResult()
    }

    override suspend fun detailsTVShow(idString: String): Result<String, FuelError> {
        return fuelInstance.request(TMDBDataRouter.DetailsTVShows(idString)).awaitStringResult()
    }
}